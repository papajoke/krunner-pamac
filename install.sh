#!/usr/bin/bash

echo "ps -faux  | grep -E 'pamac|pacman|krunner'"

sudo cp resources/org.manjaro.pamac.krunner.service /usr/share/dbus-1/services/
sudo cp resources/pamac-krunner.service /usr/lib/systemd/user/
sudo cp resources/plasma-runner-pamac.desktop /usr/share/kservices5/
sudo cp plugin/krunner_pamac.py /usr/lib/qt/plugins/
sudo chmod +x /usr/lib/qt/plugins/krunner_pamac.py

if [[ "$1" == "-l" ]]; then
    sudo rm /usr/share/dbus-1/services/org.manjaro.pamac.krunner.service
    sudo rm /usr/lib/qt/plugins/krunner_pamac.py
    sudo rm /usr/lib/systemd/user/pamac-krunner.service
    echo "python plugin/krunner_pamac.py # load dbus server in other terminal"
    #python plugin/krunner_pamac.py &
fi

if [[ "$1" == "-r" ]]; then
    sudo rm -v /usr/share/dbus-1/services/org.manjaro.pamac.krunner.service
    sudo rm -v /usr/share/kservices5/plasma-runner-pamac.desktop
    sudo rm -v /usr/lib/systemd/user/pamac-krunner.service
    sudo rm -v /usr/lib/qt/plugins/krunner_pamac.py
    ps -aux | grep 'krunner'
fi

kquitapp5 krunner
#kstart5 krunner
qdbus org.kde.krunner /App org.kde.krunner.App.querySingleRunner kpamac pacman
