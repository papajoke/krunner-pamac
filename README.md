### Krunner search packages with pamac

![alt text](./acapture_krun21.png "screen")

open pamac at package page

---

### option

for search only in package name:

run `kate ~/.config/krunnerrc &` and add

```
[kpamac]
desc=false
```

for activate this plugin only by prefix ex:`pa:pacman`

```
[kpamac]
prefix=pa
```

---

### install

After install run `kquitapp5 krunner` if not reboot
