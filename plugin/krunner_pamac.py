#!/bin/env python3

"""
https://cgit.kde.org/krunner.git/plain/src/data/org.kde.krunner1.xml
https://api.kde.org/frameworks/krunner/html/runnercontext_8cpp_source.html#l00374
https://techbase.kde.org/Development/Tutorials/Plasma4/AbstractRunner

"""

# qdbus org.kde.krunner /App org.kde.krunner.App.querySingleRunner kpamac pacman

from pathlib import Path
from dataclasses import dataclass
import configparser
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
import gi
gi.require_version('Pamac', '9.0')
from gi.repository import Pamac
from gi.repository import GLib


DBusGMainLoop(set_as_default=True)

objpath = "/kpamac"
iface = "org.kde.krunner1"

@dataclass
class Package:
    """ minimal alpm package """
    name: str
    version: str
    desc: str
    url: str
    ico: str
    relevance: float

    def export(self, want_url: bool = False):
        """format for krunner"""
        return (
            self.url if want_url else self.name,
            f"{self.name}   {self.version[:12]}",
            self.ico,
            32,    # type : https://api.kde.org/frameworks/krunner/html/runnercontext_8h_source.html
            self.relevance,
            {"subtext": self.desc}
        )

class Runner(dbus.service.Object):
    def __init__(self):
        """ set dbus and load alpm """
        dbus.service.Object.__init__(self, dbus.service.BusName("org.manjaro.pamac.krunner", dbus.SessionBus()), objpath)
        config = Pamac.Config(conf_path="/etc/pamac.conf")
        config.set_enable_aur(False)
        self.db = Pamac.Database(config=config)
        self.desc = True       # search in descriptions and names
        self.prefix = ""
        inirc = configparser.ConfigParser()
        inirc.read(f"{Path.home()}/.config/krunnerrc")
        try:
            self.desc = False if inirc['kpamac']['desc'].lower() == "false" else True
        except KeyError:
            pass
        try:
            self.prefix = inirc['kpamac']['prefix'].strip().lower()
            if self.prefix:
                self.prefix = f"{self.prefix}:"
        except KeyError:
            pass


    def _getpkgs(self, query: str):
        """ find packages by name and description """

        def setpkg(pkg):
            return Package(
                pkg.get_name(),
                pkg.get_version(),
                pkg.get_desc(),
                pkg.get_url(),
                "system-software-install" if pkg.get_reason() else "",
                self._setrelevance(pkg, query)
            )

        if self.desc:
            for pkg in self.db.search_pkgs(query):
                yield setpkg(pkg)
        else:
            for repo in self.db.get_repos_names():
                for pkg in self.db.get_repo_pkgs(repo):
                    if query in pkg.get_name():
                        yield setpkg(pkg)

    def _setrelevance(self, pkg: Package, query: str)->float:
        """ list order, display only top 10/20 by relevance """
        mini = 0.01
        relevance = mini
        name = pkg.get_name()
        if query == name:
            relevance = 1
        elif name.startswith(query) and not "-i18n" in name:
            relevance = 0.4
        else:
            relevance = 0.2 if query in name else mini
            if "-i18n" in name:
                relevance = mini
        return relevance

    @dbus.service.method(iface, in_signature='s', out_signature='a(sssida{sv})')
    def Match(self, query: str):
        """ get the matches and returns a packages list """
        query = query.strip().lower()
        if self.prefix:
            if not query.startswith(self.prefix):
                return []
            query = query[len(self.prefix):].lstrip()
        if len(query) < 3:
            return []

        ret = []
        for pkg in self._getpkgs(query):
            ret.append(pkg.export(False))
        return ret

    @dbus.service.method(iface, in_signature='ss')
    def Run(self, data: str, _action_id: str):
        """ click on item, use dbus because plugin is released after 2 minutes """
        action = dbus.Interface(
            dbus.SessionBus().get_object(
                "org.manjaro.pamac.manager",
                "/org/manjaro/pamac/manager"
            ),
            "org.gtk.Actions"
        )
        action.Activate("details", [data], [])  # args : sava{sv}


runner = Runner()
loop = GLib.MainLoop()
loop.run()
